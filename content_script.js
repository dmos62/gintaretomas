
var gintares =
["988835_10202085558579746_1194262941455578358_n.jpg",
  "1461503_894188623926414_1939580585183050457_n.jpg",
  "10300497_10201160438492322_9055649431420112663_n.jpg",
  "10360462_986400728038536_2020019086524103129_n.jpg",
  "10452990_314496542060866_3719262465119805690_o.jpg",
  "10494453_314496292060891_7368495862365426969_o.jpg",
  "10624799_715239915228165_8459340798287919554_n.jpg",
  "10749924_905812516097358_7582570484288529784_o.jpg",
  "11224579_1029543280390947_7749537981193518642_n.jpg",
  "12190093_1107902495888358_9110165880574001363_n.jpg",
  "12232660_1183512975009400_6837694905283372731_o.jpg"]
  .map(uri => "gintares/" + uri)
  .map(uri => chrome.extension.getURL(uri))

var tomai =
["401552_329924313698461_747858948_n.jpg",
"578669_472541052770119_259746526_n.jpg",
"603794_972122432811976_8475589323285075843_n.jpg",
"1015605_609451479079075_806416888_o.jpg",
"1397317_694179383939617_498716222_o.jpg",
"1548027_738320636192158_1727418890_o.jpg",
"10257320_775558245801730_2629417487319777641_o.jpg",
"11334116_1009065672450985_6757523202500675803_o.jpg",
"11870941_1049708575053361_496710008538571599_n.jpg",
"12240077_1096971686993716_2319391987247396955_n.jpg",
"12291943_1183513528342678_5708439485690661350_o.jpg"]
  .map(uri => "tomai/" + uri)
  .map(uri => chrome.extension.getURL(uri))

function random_item(items){
  return items[~~(Math.random() * items.length)];
}

function Pupsiai(pirmi, antri) {
  var that = this
  this.isrinkNuotrauka = function() {
    return random_item((Math.random() < 0.5) ? pirmi : antri)
  }
  this.nuotraukuKeitejas = function(img, ix, _) {
    img.src = that.isrinkNuotrauka()
  }
}

function walk_texts(node, fn) {
  var child, next;

  switch ( node.nodeType ) {
    case 1:  // Element
    case 9:  // Document
    case 11: // Document fragment
      child = node.firstChild;
      while ( child ) {
        next = child.nextSibling;
        walk_texts(child, fn);
        child = next;
      }
      break;

    case 3: // Text node
      fn(node);
      break;
  }
}

function zodziuKeitejas(node) {
  var text = node.nodeValue;
  text = text.replace(/\bAdam\b/g, "Tomas");
  text = text.replace(/\badam\b/g, "tomas");
  text = text.replace(/\bADAM\b/g, "TOMAS");
  text = text.replace(/\bEve\b/g, "Gintarė");
  text = text.replace(/\beve\b/g, "gintarė");
  text = text.replace(/\bEVE\b/g, "GINTARĖ");

  text = text.replace(/\bAdomas\b/g, "Tomas");
  text = text.replace(/\badomas\b/g, "tomas");
  text = text.replace(/\bADOMAS\b/g, "TOMAS");
  text = text.replace(/\bIeva\b/g, "Gintarė");
  text = text.replace(/\bieva\b/g, "gintarė");
  text = text.replace(/\bIEVA\b/g, "GINTARĖ");

  text = text.replace(/\bClyde\b/g, "Tomas");
  text = text.replace(/\bclyde\b/g, "tomas");
  text = text.replace(/\bCLYDE\b/g, "TOMAS");
  text = text.replace(/\bBonnie\b/g, "Gintarė");
  text = text.replace(/\bbonnie\b/g, "gintarė");
  text = text.replace(/\bBONNIE\b/g, "GINTARĖ");

  text = text.replace(/\bRomeo\b/g, "Tomas");
  text = text.replace(/\bromeo\b/g, "tomas");
  text = text.replace(/\bROMEO\b/g, "TOMAS");
  text = text.replace(/\bJuliet\b/g, "Gintarė");
  text = text.replace(/\bjuliet\b/g, "gintarė");
  text = text.replace(/\bJULIET\b/g, "GINTARĖ");

  text = text.replace(/\bDžuljeta\b/g, "Gintarė");
  text = text.replace(/\bdžuljeta\b/g, "gintarė");
  text = text.replace(/\bDŽULJETA\b/g, "GINTARĖ");

  text = text.replace(/\bDžiuljeta\b/g, "Gintarė");
  text = text.replace(/\bdžiuljeta\b/g, "gintarė");
  text = text.replace(/\bDŽIULJETA\b/g, "GINTARĖ");

  text = text.replace(/\bBatman\b/g, "Tomas");
  text = text.replace(/\bbatman\b/g, "tomas");
  text = text.replace(/\bBATMAN\b/g, "TOMAS");
  text = text.replace(/\bRobin\b/g, "Gintarė");
  text = text.replace(/\brobin\b/g, "gintarė");
  text = text.replace(/\bROBIN\b/g, "GINTARĖ");

  text = text.replace(/\bSuperman\b/g, "Tomas");
  text = text.replace(/\bsuperman\b/g, "tomas");
  text = text.replace(/\bSUPERMAN\b/g, "TOMAS");
  text = text.replace(/\bWonder\swoman\b/g, "Gintarė");
  text = text.replace(/\bWonder\sWoman\b/g, "Gintarė");
  text = text.replace(/\bwonder\swoman\b/g, "gintarė");
  text = text.replace(/\bWONDER\sWOMAN\b/g, "GINTARĖ");

  node.nodeValue = text;
}

function magija() {

  var pupsiai = new Pupsiai(gintares, tomai);

  Array
    .from(document.getElementsByTagName("img"))
    .forEach(pupsiai.nuotraukuKeitejas)
    walk_texts(document.body, zodziuKeitejas);

  var newtab_q =
    document.URL.startsWith("https://www.google.com/_/chrome/newtab");

  if (newtab_q) {
    var sheet = document.styleSheets[0];
    sheet.insertRule(
        "body { background-image: url(\"" + pupsiai.isrinkNuotrauka() + "\") !important; background-size: cover !important;}",
        sheet.cssRules.length
        )
  }
}

function klausk(kl, cb) {
  chrome.runtime.sendMessage(
      {klausimas: kl},
      function (resp) {
        cb(resp.atsakymas);
      }
      )
}

klausk( "paleidziu", (resp) => { (resp == true) && magija() } );
