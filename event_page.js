console.log("event page rendered.");

var Storage = {}

Storage.read = function (key, cb) {
  chrome.storage.local.get(key,
      function (obj) {
        var val = obj[key]
        console.log("reading " + key + ": " + val);
        cb(val);
      })
}

Storage.write = function (key, val) {
  console.log("writing " + key + ": " + val);
  var obj = {};
  obj[key] = val;
  chrome.storage.local.set(obj)
}

chrome.storage.onChanged.addListener(function(changes, namespace) {
  for (key in changes) {
    var storageChange = changes[key];
    console.log('Storage key "%s" in namespace "%s" changed. ' +
        'Old value was "%s", new value is "%s".',
        key,
        namespace,
        storageChange.oldValue,
        storageChange.newValue);
  }
});

function now() { return (new Date).getTime() }

function isAfter(date) { return date < now() }

chrome.runtime.onMessage.addListener(
    (msg, _, sendResponse) => {
      console.log("received message: " + msg.klausimas);
      if (msg.klausimas == "paleidziu") {
        Storage.read("offUntil",
            offUntil => {
                if (isOn(offUntil)) {
                  console.log("already on.");
                  sendResponse({atsakymas: true});
                } else if (isAfter(offUntil)) {
                  console.log("off, but after offUntil.");
                  turnOn();
                  sendResponse({atsakymas: true});
                }
                else {
                  setIconOff();
                  sendResponse({atsakymas: offUntil});
                }
            })
        return true;
      }
    })

var offIcon = "icon19-off.png";

var onIcon = "icon19-on.png";

function turnOff() {
  console.log("turning off.")
  Storage.write("offUntil", now() + 60*60*1000);
  chrome.browserAction.setIcon({path: offIcon})
}

function turnOn() {
  console.log("turning on.")
  Storage.write("offUntil", false)
  chrome.browserAction.setIcon({path: onIcon})
}

function isOn(offUntil) { return offUntil == undefined || offUntil == false }

function flickSwitch(offUntil) {
  isOn(offUntil) ? turnOff() : turnOn();
}

function setIconOn() {
  chrome.browserAction.setIcon({path: onIcon})
}

function setIconOff() {
  chrome.browserAction.setIcon({path: offIcon})
}

function onClick() { Storage.read("offUntil", flickSwitch) }

chrome.browserAction.onClicked.addListener(onClick);
